package com.company;


public class Main {


    public static void print(Item item){
        item.print();
    }

    public static void main(String[] args) {

        print(new Square());
        print(new Another());

    }
}
